# Clearscore website

## Score Indicator Component

This react component connects to a redux store for its data as the creditReportInfo prop. This is fetched immediately in the Dashboard component.

The slide changes automatically every 5 seconds by changing the left property on the slide's parent. The animation happens by css.

### Todo:

* finish styling
* test displayCurrency component
* add ability to stop the carousel (WCAG 2 requirement)
* add click event for advances the slider
* improve perf on mobile by using translate instead of left property