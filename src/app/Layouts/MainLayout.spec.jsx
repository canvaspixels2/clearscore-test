import React from 'react';
import MainLayout from './MainLayout.jsx';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer
    .create(<MainLayout><p>Foo</p></MainLayout>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});