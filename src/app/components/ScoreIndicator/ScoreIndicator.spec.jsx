import React from 'react';
import { ScoreIndicator, delay } from './ScoreIndicator.jsx';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

describe('ScoreIndicator', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<ScoreIndicator creditReportInfo={{
        score: 100,
        maxScoreValue: 700,
        currentShortTermCreditLimit: 300000
      }} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe('componentDidMount()', () => {
    it('calls setTimeout', () => {
      const component = shallow(<ScoreIndicator creditReportInfo={{
          score: 100,
          maxScoreValue: 700,
          currentShortTermCreditLimit: 300000
        }} />);
      global.setTimeout = jest.fn();
      const instance = component.instance();
      instance.componentDidMount();
      expect(global.setTimeout).toHaveBeenCalledWith(instance.updateXPos, delay);
    });
  });

  describe('updateXPos()', () => {
    it('calls setState', () => {
      const component = shallow(<ScoreIndicator creditReportInfo={{
          score: 100,
          maxScoreValue: 700,
          currentShortTermCreditLimit: 300000
        }} />);
      const instance = component.instance();
      instance.setState = jest.fn();
      instance.updateXPos();
      expect(instance.setState).toHaveBeenCalledWith({ xPos: -300 });
    });

    it('calls setTimeout', () => {
      const component = shallow(<ScoreIndicator creditReportInfo={{
          score: 100,
          maxScoreValue: 700,
          currentShortTermCreditLimit: 300000
        }} />);
      global.setTimeout = jest.fn();
      const instance = component.instance();
      instance.updateXPos();
      expect(global.setTimeout).toHaveBeenCalledWith(instance.updateXPos, delay);
    });
  });
});