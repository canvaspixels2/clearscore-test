import React from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import displayCurrency from '../../utils/displayCurrency';

import './scoreIndicator.scss';

const mapStateToProps = (state) => {
  return {
    creditReportInfo: state.creditReportInfo
  };
};

const slideWidth = 300;
let slideIndex = 0;
const numberOfSlides = 2;
export const delay = 5000;

export class ScoreIndicator extends React.Component {
  componentDidMount() {
    global.setTimeout(this.updateXPos, delay);
  }

  state = {
    xPos: 0
  }

  updateXPos = () => {
    if (slideIndex < numberOfSlides - 1) {
      slideIndex += 1;
    } else {
      slideIndex = 0;
    }
    this.setState({ xPos: -(slideIndex * slideWidth) });

    global.setTimeout(this.updateXPos, delay);
  }

  render() {
    const { creditReportInfo } = this.props;

    return (
      <div className="indicator">
        <div className="indicator__slides" style={{ left: this.state.xPos }}>
          <div className="indicator__slide">
            <div className="indicator__slide__inner">
              <div>
                <span className="indicator__score-is">Your credit score is</span>
                <span className="indicator__score">{creditReportInfo.score}</span>
                <span className="indicator__max">out of {creditReportInfo.maxScoreValue}</span>
              </div>
            </div>
          </div>
          <div className="indicator__slide">
            <div className="indicator__slide__inner">
              <div>
                <div>
                  <span>Your long term debt total</span>
                </div>
                <div>Total credit limit: {displayCurrency(creditReportInfo.currentShortTermCreditLimit)}</div>
                <div className="indicator__status">No change from last month</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

ScoreIndicator.propTypes = {
  creditReportInfo: PropTypes.shape({
    score: PropTypes.number,
    maxScoreValue: PropTypes.number,
    currentShortTermCreditLimit: PropTypes.number,
  }).isRequired
}

export default connect(mapStateToProps)(ScoreIndicator);
