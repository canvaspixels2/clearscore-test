import React from 'react';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import getCreditReportInfo from '../../redux/actions/getCreditReportInfo';
import rootReducer from '../../redux/rootReducer';
import './dashboard.scss';
import ScoreIndicator from '../ScoreIndicator/ScoreIndicator';

const store = createStore(rootReducer, applyMiddleware(thunk));
store.dispatch(getCreditReportInfo());

export default () => (
  <Provider store={store}>
    <ScoreIndicator />
  </Provider>
);
