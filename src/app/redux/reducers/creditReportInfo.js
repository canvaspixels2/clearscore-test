export default (state = {}, action) => {
  if (action.type === 'CREDIT_REPORT_INFO_SUCCESS') {
    return action.data;
  }
  return state;
}