import creditReportInfo from './creditReportInfo';
import * as fetch from '../../utils/fetch';

describe('creditReportInfo() reducer', () => {
  it('returns original state if type not CREDIT_REPORT_INFO_SUCCESS', () => {
    const state = {};
    expect(creditReportInfo(state, { type: 'FOO' })).toBe(state);
  });

  it('returns new state if type is CREDIT_REPORT_INFO_SUCCESS', () => {
    const state = {};
    expect(creditReportInfo(state, {
      type: 'CREDIT_REPORT_INFO_SUCCESS',
      data: 'somedata',
    })).toBe('somedata');
  });
});