import creditReportInfo from './reducers/creditReportInfo';
import { combineReducers } from 'redux';

export const reducer = {
  creditReportInfo
};

export default combineReducers(reducer);