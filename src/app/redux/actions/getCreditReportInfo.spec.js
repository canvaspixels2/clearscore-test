import getCreditReportInfo from './getCreditReportInfo';
import * as fetch from '../../utils/fetch';

describe('getCreditReportInfo()', () => {
  it('calls getJSON', () => {
    fetch.getJSON = jest.fn().mockImplementation(() => Promise.resolve({ creditReportInfo: 'someData' }));
    const dispatchStub = jest.fn();
    getCreditReportInfo()(dispatchStub);
    expect(fetch.getJSON).toHaveBeenCalledWith('https://s3.amazonaws.com/cdn.clearscore.com/native/interview_test/creditReportInfo.json');
  });

  it('calls dispatch if getJSON returns data', (done) => {
    fetch.getJSON = jest.fn().mockImplementation(() => Promise.resolve({ creditReportInfo: 'someData' }));
    const dispatchStub = jest.fn();
    getCreditReportInfo()(dispatchStub).then(() => {
      expect(dispatchStub).toHaveBeenCalledWith({
        type: 'CREDIT_REPORT_INFO_SUCCESS',
        data: 'someData'
      });
      done();
    });
  });

  it('doesn’t call dispatch if getJSON rejects', (done) => {
    fetch.getJSON = jest.fn().mockImplementation(() => Promise.reject('some error'));
    const dispatchStub = jest.fn();
    getCreditReportInfo()(dispatchStub).then(() => {
      expect(dispatchStub).not.toHaveBeenCalled();
      done();
    });
  });
});