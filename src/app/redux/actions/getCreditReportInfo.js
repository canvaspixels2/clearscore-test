import { getJSON } from '../../utils';

export default () => (dispatch) =>
  getJSON('https://s3.amazonaws.com/cdn.clearscore.com/native/interview_test/creditReportInfo.json')
    .then((data) => {
      dispatch({
        type: 'CREDIT_REPORT_INFO_SUCCESS',
        data: data.creditReportInfo
      });
    })
    .catch((err) => {
      console.log('Something went wrong, display error to user');
      console.log(err);
    });